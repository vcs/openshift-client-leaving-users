#Because of users leaving CERN, we have to perform some cleanup on the projects they were handling.
#Users are blocked after they leave the CERN due to their identities. We cannot delete blocked accounts because deleting (destroy) a user also removes all contributions such as comments (see warning when destroying a user from the admin area).
#We'll want to keep issues and comments left by a user after he/she leaves cern, so we can't destroy the Gitlab account if there is such data left behind. 

#Enumerate expired accounts to be processed. Expired accounts will necessarily be Blocked in GitLab (but not all Blocked will be expired). 
#Archive all personal projects. It seems easier to simply use the Archive function in GitLab, as projects can be recovered without restoring from backup (we can always delete later). 
#Mark all personal projects Archived, Private and remove all members.
#Reset email to account_name@cern.ch. The account name cannot be reused by someone else (or the same person later) so there cannot be conflicts but the email address can be reused by a new user. And GitLab will check for email address conflicts.
#Remove the GitLab user from all Groups and Projects he's member of. If the user is the last owner of a Group, we need to find a new owner before we can remove the last owner (TODO)
#To know an expired account has been processed we will remove its LDAP/SAML identities after cleanup is done so it is not processed again.

def get_new_owner(group)
  return nil
end

def cleanup_user(user)
  #In case one user has a different provider, we use the one from the ldap_identity
  provider = user.ldap_identity.provider
  config = Gitlab::LDAP::Config.new(provider)
  ldap = Net::LDAP.new(config.adapter_options)

  if ldap.bind
    Rails.logger.info("[user_cleanup] Authentication succeeded: " + user.username)
  
    dn=user.ldap_identity.extern_uid
    results = ldap.open{|ldap| ldap.search(scope: Net::LDAP::SearchScope_BaseObject, base: dn, attributes: %w{dn}) }
    #If we did not get any result, account does not exist in AD anymore
    if results.nil?
      response = ldap.get_operation_result

      unless response.code.zero?
        Rails.logger.warn("[user_cleanup] LDAP search error: #{response.message}")
      end        
    else
      #We archive all the personal projects
      user.personal_projects.map{|proj| proj.archive!; Rails.logger.warn("[user_cleanup] Archiving " + proj.name);}
      #We mark the projects as private and remove all the other members BUT the owner
      user.personal_projects.map{|proj| proj.members.map{|member| 
        proj.visibility_level=0 
        proj.save 
        Rails.logger.warn("[user_cleanup] Changing visibility to private and deleting non-owner members: " + proj.name)
        if (proj.owner != member.user)
         member.delete end  
        } 
      }

      #We reset the email to account_name@cern.ch (See VCS 698)
      user.email = user.username + "@cern.ch"
      user.skip_reconfirmation!
      user.save
      #Changing the primary email adds it to the secondary email list, so we need to remove them as well.
      user.emails = []
      user.save
      Rails.logger.warn("[user_cleanup] User " + user.username + " email changed to " + user.email)

      #We remove the user from all groups and projects he is member of.
      #BE CAREFUL, THROUGH THE CONSOLE A PROJECT CAN BE SET ORPHAN
      #If he was the last owner, find a new one before. (WIP: owner history)

      #If the user can leave the project, we delete the current user from the member list
      project_membership = user.projects
      project_membership.map{|proj| if user.can_leave_project?(proj) then proj.project_member(user).delete end}

      #Groups where the user is the last owner. They have to be reassigned
      solo_owned_groups = user.solo_owned_groups
      for solo_owned_group in solo_owned_groups do
        new_owner = get_new_owner(solo_owned_group)
        if new_owner != nil
          Rails.logger.info("[user_cleanup] Reassigning group " + solo_owned_group.name + " from " + user.username + " to user " + new_owner.username)
          solo_owned_group.add_owner(new_owner)
          solo_owned_group.save
          #Since we do not have the updated solo_owned list after the save, we remove it manually.
          solo_owned_groups.delete(solo_owned_group)
        end
      end

      #If this user is not the last owner, remove it from the group
      user.groups.map{|user_group| if (not solo_owned_groups.include?(user_group)) then user_group.group_member(user).delete; Rails.logger.warn("[user_cleanup] Removing user " + user.username + " from group: " + user_group.name); end }
      #TODO ADD GROUP OWNERSHIP REASSIGNATION
      #We temporary print on the logger the groups that need manual reassignment
      solo_owned_groups.map{|solo_owned_group| Rails.logger.info("[user_cleanup] Requires manual group reassignment [User: " + user.username + " Group: " + solo_owned_group.name + "]")}


      #We delete the user identities (WIP: WE ONLY REMOVE IDENTITIES WHEN THE COMPLETE GROUP REASSIGNEMENT HAS BEEN DONE)
      if solo_owned_groups.length == 0
        Rails.logger.info("[user_cleanup] Cleanup for user finished, deleting identities: " + user.username)
        user.identities.map(&:delete)
      end
    end
  else
    Rails.logger.info("[user_cleanup] Authentication failed: " + user.username)
    #We couldn't bind, we skip the account
  end
end


#retrieving the blocked accounts
params = Hash.new
params[:blocked] = true
users = UsersFinder.new(nil, params).execute
i = 1
for user in users do
  #We need to check if the account already had their identities removed
  Rails.logger.info("[user_cleanup] Cleanup progress: " + user.username + " [" + i + "/" + users.length + "]")
  if user.ldap_identity != nil and user.ldap_identity.extern_uid != nil and user.ldap_identity.extern_uid != ''
    #We extract the provider from the identity itself instead of Gitlab::LDAP::Config.available_servers.first['provider_name']
    begin
      cleanup_user(user)
    rescue Net::LDAP::Error => error
      Rails.logger.warn("[user_cleanup] LDAP search raised exception #{error.class}: #{error.message}")
      #We cannot cleanup this account
    rescue Timeout::Error
      Rails.logger.warn("[user_cleanup] LDAP search timed out after #{config.timeout} seconds")
      #We cannot cleanup this account
    rescue NoMethodError => error
      Rails.logger.warn("[user_cleanup] This user caused an exception #{error.class}: #{error.message}")
      #We cannot cleanup this account
    end
  end
  i = i + 1
end